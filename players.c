#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linkedList.h"

typedef struct Player* APlayer;

/*Player type that keeps track of the player type being 'a' for computer
 *and 'h' for stdin (human). Using a deck struct as a hand.
 */
struct Player {
    char playerType;
    struct Deck hand;
    int playerNum;
};

//Initialize a player with the type of player and a hand of 5 cards
//from the deck.
APlayer init_player(char playerType, ADeck deck, bool newGame) {
    APlayer thisPlayer = malloc(sizeof(struct Player));
    thisPlayer->playerType = playerType;
    init_deck(&thisPlayer->hand);
    char* cardNum = (char*)malloc(sizeof(char*));
    char* cardSuit = (char*)malloc(sizeof(char*));
    if (newGame) {
        for (int i = 0; i < 5; i++) {
            get_card_data(deck, cardNum, cardSuit);
            add_card(&thisPlayer->hand, *cardNum, *cardSuit);
        }
    }/* else {
        get_saved_hand_data(&thisPlayer->hand, loadLine);
    }*/
    free(cardNum);
    free(cardSuit);
    return thisPlayer;
}

/*
 *This will output a player hand as a String without spaces.
 */
void print_player_hand_save(char* hand, FILE* fileType) {
    int i = 0;
    while (hand[i] != '\0') {
        fprintf(fileType, "%c", hand[i]);
        i++;
    }
    fprintf(fileType, "\n");
    free(hand);
}

/*
 *This will print the players hand as a space seperated line of cards
 */
void print_player_hand(char* hand) {
    int i = 0;
    char* thisHand = hand;
    while (thisHand[i] != '\0' || thisHand[(i + 1)] != '\0') {
        printf("%c%c", hand[i], hand[i + 1]);
        i += 2;
        if(hand[i] != '\0') {
            printf(" ");
        }
    }
    printf("\n");
    free(thisHand);
}

