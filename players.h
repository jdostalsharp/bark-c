typedef struct Player* APlayer;

struct Player {
    char playerType;
    struct Deck hand;
    int playerNum;
};

APlayer init_player(char playerType, ADeck deck, bool newGame);

void print_player_hand_save(char* hand, FILE* fileType);

void print_player_hand(char* hand);

