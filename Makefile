OPTS = --std=c99 -pedantic -Wall -g
RM = rm -rf

bark: bark.o linkedList.o players.o
		gcc $(OPTS)  bark.o linkedList.o players.o -o bark

bark.o: bark.c linkedList.h players.h
		gcc $(OPTS) -c  bark.c

linkedList.o: linkedList.c linkedList.h
		gcc $(OPTS) -c linkedList.c

players.o: players.c players.h linkedList.h
		gcc $(OPTS) -c players.c

clean:
		$(RM) bark *.o *~


