#include <stdio.h>
#include <stdlib.h>

typedef struct Card* CardNode;

typedef struct Deck* ADeck;

//This holds the pointer to the head and tail of a deck of cards.
struct Deck {
    CardNode head;
    CardNode tail;
};

//This is a struct to define a card in a deck of cards with a pointer to
//the next card in the deck
struct Card {
    char cardNum;
    char cardSuit;
    struct Card* next;
    struct Card* previous;
};

/*Initialises a deck with a pointer to the head and a pointer to the tail.
 * To initialise the deck you need to first create a struct Deck [deckName]
 * Then call init_deck(&[deckName])
 */
void init_deck(ADeck deck) {
    deck->head = 0;
    deck->tail = 0;
}

//Free a node of the deck when done with that node
//Sets head to the next node.
void free_deck(ADeck deck) {
    CardNode top = deck->head;
    while(top != 0) {
        CardNode temp = top;
        top = top->next;
        free(temp);
    }
}

//Adds a card to the end of the deck and changes tail.
void add_card(ADeck deck, char cardNum, char cardSuit) {
    if (deck->head == 0) {
        deck->tail = deck->head = malloc(sizeof(struct Card));
        deck->head->next = 0;
        deck->head->previous = 0;
    } else {
        CardNode card = malloc(sizeof(struct Card));
        card->next = 0;
        card->previous = deck->tail;
        deck->tail->next = card;
        deck->tail = card;
    }
    deck->tail->cardNum = cardNum;
    deck->tail->cardSuit = cardSuit;
}

//Pops a card from the front of the deck and sets the pointers cardNum
//and cardSuit to the values the card holds.
//frees the card that was popped
//returns 0 if successful or 3 if end of deck.
int get_card_data(ADeck deck, char* cardNum, char* cardSuit) {
    if(deck->head == 0) {
        return 3;
    }
    *cardNum = deck->head->cardNum;
    *cardSuit = deck->head->cardSuit;
    CardNode temp = deck->head;
    deck->head = deck->head->next;
    deck->head->previous = 0;
    free(temp);
    if(deck->head == 0) {
        deck->tail = 0;
    }
    return 0;
}

/*
 *Initialises a players hand from a savedGame
 */
void get_saved_hand_data(ADeck deck, char* handLine) {
    int i = 0;
    char* cardNum = (char*)malloc(sizeof(char));
    char* cardSuit = (char*)malloc(sizeof(char));
    while (i <= 12) {
        *cardNum = handLine[i];
        i++;
        *cardSuit = handLine[i];
        if (*cardNum != ' ' && *cardSuit != ' ') {
            add_card(deck, *cardNum, *cardSuit);
        }
        i++;
    }
}

//Gets details of a card and assigns them to cardNum and cardSuit poiters
//from position then removes card and links the card structs on either side.
//Returns 0 if succesful.
int get_card_at_position(ADeck hand, int position,
        char* cardNum, char* cardSuit) {
    if (hand->head == 0) {
        return 3;
    }
    CardNode temp = hand->head;
    if (position > 1) {
        for (int i = 1; i < position; i++) {
            if(temp->next == 0) {
                return 3;
            }
            temp = temp->next;
        }
    }
    *cardNum = temp->cardNum;
    *cardSuit = temp->cardSuit;
    if (temp->previous == 0) {
        temp->next->previous = 0;
        hand->head = temp->next;
    } else if (temp->next == 0) {
        temp->previous->next = 0;
        hand->tail = temp->previous;
    } else {
        temp->next->previous = temp->previous;
        temp->previous->next = temp->next;
    }
    free(temp);
    if(hand->head == 0) {
        hand->tail = 0;   
    }
    return 0;
}  

/* A function to return the char* with the values of a deck struct.
 */
char* get_hand_string(ADeck deck) {
    int size = 44;
    int atTail = 0;
    int cardCount = 0;
    if(deck->head == 0) {
        return '\0'; 
    }
    char* hand = (char*)malloc(sizeof(char) * size);
    CardNode temp = deck->head;
    while(!atTail) {
        for (; atTail != 1; cardCount++) {
            char inputNum = temp->cardNum;
            char inputSuit = temp->cardSuit;
            if (temp->next == 0) {
                atTail = 1;
            }
            if (cardCount == (size - 2)) {
                size *= 2;
                hand = realloc(hand, size);
            }
            hand[cardCount] = inputNum;
            cardCount++;
            hand[cardCount] = inputSuit;
            temp = temp->next;
        }
    }
    hand[cardCount] = '\0';
    return hand;
}
