#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "linkedList.h"
#include "players.h"

/* Struct to hold the game variables in, reducing the need for global
 * variables 
 */
#ifndef _GAME_DETAILS
#define _GAME_DETAILS
struct GameDetails {
    int firstMove;
    int width;
    int height;
    int cardsDrawn;
    int deckSize;
    int currentPlayer;
    char** gameBoard;
    struct Deck deck;
    char* deckFileName;
    int loadGame;
    APlayer player1;
    APlayer player2;
};

#endif



// Function declarations

void check_new_game_args(char** argv, struct GameDetails* game);

int check_file(FILE* deck);

char* read_line(FILE* file);

char** create_board(int width, int height);

void print_board(char** gameboard, int width, int height);

void check_conditions(int argc, char** argv, struct GameDetails* game,
        FILE* deck);

void get_deck(struct GameDetails* game, FILE* deckFile);

void testing_deck(struct Deck deck); 

void init_game(FILE* deckFile, struct GameDetails* game, char** argv);
void print_players_turn(struct GameDetails* game);

int put_card_into_game(struct GameDetails* game, int cardLocation,
        int widthLocation, int heightLocation, bool cpuPlayer);

int get_player_turn(struct GameDetails* game); 

int check_adjacent(struct GameDetails* game, int widthPosition,
        int heightPosition);

void cpu_player1_logic(struct GameDetails* game, int* width, int* height);

void cpu_player2_logic(struct GameDetails* game, int* width, int* height);

bool check_free_space(struct GameDetails* game);

void free_game_stuff(struct GameDetails* game);

void save_game(struct GameDetails* game, char* saveGameName);

int check_save_input(char* name);

void print_board_save(struct GameDetails* game, FILE* fileType);

void load_game(struct GameDetails* game, char** argv);

int check_within_board(struct GameDetails* game, int height, int width);

int cpu_player_turn(struct GameDetails* game, int* width, int* height);


//Main game function.
int main(int argc, char** argv) {
    struct GameDetails* game = malloc(
            sizeof(struct GameDetails));
    FILE* deckFile = fopen(argv[1], "r");
    (check_conditions(argc, argv, game, deckFile));
    if (argc == 6) { 
        init_game(deckFile, game, argv);
        game->loadGame = 0;
    } else {
        load_game(game, argv);
        game->loadGame = 1;
    }
    print_board(game->gameBoard, game->width, game->height);
    while (game->cardsDrawn < (game->deckSize - 1) &&
            check_free_space(game) == true) {
        print_players_turn(game); 
        print_board(game->gameBoard, game->width, game->height);
    }
    fprintf(stdout, "Player 1=0 PLayer 2=0\n");
    fclose(deckFile);
    free_game_stuff(game);
    return 0;
}


/*Checks argv initialising conditions to see if they fit to spec.
 * returns the corresponding error code int if there is an error 
 * otherwise it returns 0.
 */
void check_conditions(int argc, char** argv, struct GameDetails* game,
        FILE* deck) {
    //Check correct command line arguments
    if (argc != 6 && argc != 4) {
        fprintf(stderr, "Usage: bark savefile p1type p2type\n");
        fprintf(stderr, "bark deck width height p1type p2type\n");
        exit(1);
    }
    if (argc == 6) {
        check_new_game_args(argv, game);
        if (deck == NULL) {
            fprintf(stderr, "Unable to parse deckfile\n");
            exit(3);
        }
        if (check_file(deck) == 3) {
            fprintf(stderr, "Unable to parse deckfile\n");
            exit(3);
        }
        game->deckFileName = argv[1];   
    }
    //If loading a save game file check player types
    if (argc == 4) {
        if ((*argv[2] != 'a' && *argv[2] != 'h') ||
                (*argv[3] != 'a' && *argv[3] != 'h')) {
            fprintf(stderr, "Incorrect arg types\n");
            exit(2);
        }
    }
}

//Check width and height are within boundsif we are creating a board
//then check that player types are correct
void check_new_game_args(char** argv, struct GameDetails* game) {
    if (atoi(argv[2]) < 1 || atoi(argv[2]) > 100) {
        fprintf(stderr, "Incorrect arg types\n");
        exit(2);
    }
    if (atoi(argv[3]) < 1 || atoi(argv[3]) > 100) {
        fprintf(stderr, "Incorrect arg types\n");
        exit(2);
    }
    if ((*argv[4] != 'a' && *argv[4] != 'h') ||
            (*argv[5] != 'a' && *argv[5] != 'h')) {
        fprintf(stderr, "Incorrect arg types\n");
        exit(2);
    }	
    //Double the width because each entry has a card number and suit
    game->width = (atoi(argv[2]) * 2);
    game->height = atoi(argv[3]); 
}

/*Mallocs memory for a char* array (height) of char* (width) * 2
 *Width is doubled to take both chars of a card.
 */
char** create_board(int width, int height) {
    char** gameboard = (char**)malloc(sizeof(char*) * (height));
    for (int i = 0; i < height; i++) {
        gameboard[i] = (char*)malloc(sizeof(char) * (width));   
    }
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            gameboard[i][j] = '.';
        }
    }
    return gameboard;
}

//Prints the malloced gameboard to stdout.
void print_board(char** gameBoard, int width, int height) {
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            fprintf(stdout, "%c", gameBoard[i][j]);
        }
        fprintf(stdout, "\n");
    }
}

/*Check the input deck file to make sure it meets the spec for a deck file
 *returns: 0 for good deck, 3 for any other issues with deck.
 */
int check_file(FILE* deck) {
    char* line = read_line(deck);
    int cardCount = 0;
    int deckSize = 0;
    if (line == NULL) {
        free(line);
        return 3;
    }
    deckSize = atoi(line);
    while (cardCount < deckSize) {
        if(strcmp(read_line(deck), "\0") == 0) {
            free(line);
            return 3;
        }
        cardCount++;
    }
    if (read_line(deck) != 0) {
        free(line);
        return 3;
    }
    if (cardCount != deckSize) {
        free(line);
        return 3;
    }
    free(line);
    return 0;
}

/*Function to read a line of a file at a time and return it as a string
 *with end of string '\0' attached. Returns NULL for end of file.
 */
char* read_line(FILE* file) {
    int resultLength = 10;
    char* result = malloc(sizeof(char) * resultLength);
    int position = 0;
    int next = 0;
    while (1) {
        if (position == resultLength - 1) {
            resultLength *= 2;
            result = realloc(result, resultLength);
        }
        next = fgetc(file);
        if (next == EOF) {
            result[position] = '\0';
            return NULL;
        } else if (next == '\n') {
            result[position] = '\0';
            return result;
        } else {
            result[position++] = (char)next;
        }
    }
    return result;
}

//Populates a deck struct with the cards from a verified deck.
void get_deck(struct GameDetails* game, FILE* deckFile) {
    int sizeOfDeck = atoi(read_line(deckFile));
    game->deckSize = sizeOfDeck;
    init_deck(&game->deck);
    for (int i = 0; i < sizeOfDeck; i++) {
        char* cardDetails = read_line(deckFile);
        add_card(&game->deck, cardDetails[0], cardDetails[1]);
        free(cardDetails);
    }
}

//A function to test if the deck is moving where it should be.
void testing_deck(struct Deck deck) {
    char* cardNo = (char*)malloc(sizeof(char));
    char* cardSt = (char*)malloc(sizeof(char));
    int run = 0;
    while (run != 3) {
        run = get_card_data(&deck, cardNo, cardSt);
        printf("%c%c\n", *cardNo, *cardSt);
    }
    free(cardNo);
    free(cardSt);
}

/*
 *initialize a new game
 */
void init_game(FILE* deckFile, struct GameDetails* game, char** argv) {
    game->gameBoard = create_board(game->width, game->height);
    rewind(deckFile);
    get_deck(game, deckFile);
    game->player1 = init_player(*argv[4], &game->deck, true);
    game->player2 = init_player(*argv[5], &game->deck, true);
    game->player1->playerNum = 1;
    game->player2->playerNum = 2;
    game->currentPlayer = 1;
    game->firstMove = 1;
    game->cardsDrawn = 10;
}

/*
 *Initialise a game from a savegame file.
 */
void load_game(struct GameDetails* game, char** argv) {
    FILE* loadGame = fopen(argv[1], "r");
    if (loadGame == NULL) {
        fprintf(stderr, "Unable to parse savefile");
        exit(4);
    }
    char* loadFileLine = read_line(loadGame);
    sscanf(loadFileLine, "%d %d %d %d", &game->width, &game->height,
            &game->cardsDrawn, &game->currentPlayer);
    game->width *= 2;
    game->gameBoard = create_board(game->width, game->height);
    char* deckName = read_line(loadGame);
    FILE* deck = fopen(deckName, "r");
    free(deckName);
    if (deck == NULL) {
        fprintf(stderr, "Unable to parse savefile");
        exit(4);
    }
    get_deck(game, deck);
    game->firstMove = 0;
    char* cardNum = (char*)malloc(sizeof(char));
    char* cardSuit = (char*)malloc(sizeof(char));
    for (int i = 0; i < game->cardsDrawn; i++) {
        int j = get_card_data(&game->deck, cardNum, cardSuit);
        if (j == 3) {
            fprintf(stderr, "Unable to parse savefie");
            exit(4);
        }
    }
    game->player1 = init_player(*argv[2], &game->deck, false);
    game->player2 = init_player(*argv[3], &game->deck, false);
    get_saved_hand_data(&game->player1->hand, read_line(loadGame));
    get_saved_hand_data(&game->player2->hand, read_line(loadGame));
    for (int i = 0; i < game->height; i++) {
        loadFileLine = read_line(loadGame);
        for (int j = 0; j < game->width; j++) {
            if (loadFileLine[j] == '*') {
                game->gameBoard[i][j] = '.';
            } else {
                game->gameBoard[i][j] = loadFileLine[j];
            }
        }
    }
    fclose(loadGame);
    free(loadFileLine);
    free(cardNum);
    free(cardSuit);
}

/*
 *Prints the details for current -layers turn then calls get_player_turn()
 */
void print_players_turn(struct GameDetails* game) {
    APlayer player;
    char* cardNum = (char*)malloc(sizeof(char));
    char* cardSuit = (char*)malloc(sizeof(char));
    if (game->currentPlayer == 1) {
        player = game->player1;
    } else {
        player = game->player2;
    }
    if (game->loadGame) {
        game->loadGame = 0;
    } else {
        get_card_data(&game->deck, cardNum, cardSuit);
        game->cardsDrawn++;
        add_card(&player->hand, *cardNum, *cardSuit);
    }
    if (player->playerType == 'a') {
        fprintf(stdout, "Hand: ");
    } else {
        fprintf(stdout, "Hand(%d): ", game->currentPlayer);
    }
    print_player_hand(get_hand_string(&player->hand));    
    int playerMoved = 1;
    if (player->playerType == 'a') {
        get_player_turn(game);
        playerMoved = 0;
    }
    fflush(stdout);
    while (playerMoved != 0) {
        printf("Move? ");
        playerMoved = get_player_turn(game);
    }
    if (game->currentPlayer == 1) {
        game->currentPlayer = 2;
    } else {
        game->currentPlayer = 1;
    }
    free(cardNum);
    free(cardSuit);
}

/*
 *checks to see that height and width are in the gameBoard then gets the
 *card details and inserts it into a free spot on the gameBoard. Then draws
 *a card from the deck for the player who just took the turn.
 */
int put_card_into_game(struct GameDetails* game, int cardLocation,
        int widthLocation, int heightLocation, bool cpuPlayer) {
    if (!(game->gameBoard[heightLocation - 1][(widthLocation * 2) - 1]
            == '.')) {
        return 1;
    }
    if (check_within_board(game, heightLocation, widthLocation)) {
        return 1;
    }
    APlayer player;
    if (game->currentPlayer == 1) {
        player = game->player1;
    } else {
        player = game->player2;
    }
    char* cardNum = (char*)malloc(sizeof(char));
    char* cardSuit = (char*)malloc(sizeof(char));
    int worked = 0;
    worked = get_card_at_position(&player->hand, cardLocation, cardNum,
            cardSuit);
    if (worked == 0) {
        game->gameBoard[heightLocation - 1][(widthLocation * 2) - 2] 
                = *cardNum;
        game->gameBoard[heightLocation - 1][(widthLocation * 2) - 1] 
                = *cardSuit;
    }
    if (cpuPlayer) {
        fprintf(stdout, "Player %d plays %c%c in column %d row %d\n", 
                game->currentPlayer, *cardNum, *cardSuit, 
                widthLocation, heightLocation);
    }
    free(cardNum);
    free(cardSuit);
    return worked;
}

/*
 *Gets the player input for their turn as card position, col, row. 
 *Checks to see if the card position is valid and then calls 
 *put_card_into_game.
 */
int get_player_turn(struct GameDetails* game) {
    int height, width, cardPosition, worked = 1;
    char buffer[81];
    char* saveFileName = (char*)malloc(sizeof(char) * 80);
    APlayer player;
    if (game->currentPlayer == 1) {
        player = game->player1;
    } else {
        player = game->player2;
    }
    if (player->playerType == 'a') {
        return cpu_player_turn(game, &width, &height);
    }
    if (fgets(buffer, 80, stdin) == 0) {
        free(saveFileName);
        fprintf(stderr, "End of input\n");
        exit(7);
    }
    if (!check_save_input(buffer)) {
        if (sscanf(buffer, "%s", saveFileName) != 1) {
            fprintf(stdout, "Unable to save");
        } else if (buffer[0] == EOF) {
            fprintf(stderr, "End of Input\n");
            free(saveFileName);
            exit(7);
        }
        save_game(game, saveFileName);
    } else if (sscanf(buffer, "%d %d %d", &cardPosition,
            &width, &height) != 3) {
        if (buffer[0] == '\n') {
            free(saveFileName);
            return 1;
        } else {
            free(saveFileName);
            return 1;
        }
    } else if (game->firstMove == 1) {
        worked = put_card_into_game(game, cardPosition,
                width, height, false);
        game->firstMove = 0;
        free(saveFileName);
    } else {
        if (check_adjacent(game, width, height) == 1) {
            worked = put_card_into_game(game, (cardPosition),
                    width, height, false);
            free(saveFileName);
        }
    }
    return worked;
}

/*
 *Runs the computers turn.
 */
int cpu_player_turn(struct GameDetails* game, int* width, int* height) {
    int worked;
    int thisWidth = *width;
    int thisHeight = *height;
    if (game->currentPlayer == 1) {
        cpu_player1_logic(game, &thisWidth, &thisHeight);
        worked = put_card_into_game(game, 1, thisWidth, thisHeight, true);
    } else {
        cpu_player2_logic(game, &thisWidth, &thisHeight);
        worked = put_card_into_game(game, 1, thisWidth, thisHeight, true);
    }
    game->firstMove = 0;
    return worked;
}

//Checks to see if the input is within the game board
int check_within_board(struct GameDetails* game, int height, int width) {
    if (height <= 0 || height > game->height) {
        return 1;
    }
    if (width <= 0 || ((width * 2) - 2) > game->width) {
        return 1;
    }
    return 0;
}

/*
 *Checks to see if the position the player chooses to place their card is
 *adjacent to a card already in play.
 */
int check_adjacent(struct GameDetails* game, int widthPosition,
        int heightPosition) {
    int boardWidth = (game->width);
    int boardHeight = (game->height);
    int boardWidthPos = boardWidth + ((widthPosition * 2) - 2);
    int boardHeightPos = boardHeight + (heightPosition - 1);

    if (game->gameBoard[boardHeightPos % boardHeight]
            [(boardWidthPos + 2) % boardWidth] != '.') {
        return 1;
    } else if (game->gameBoard[boardHeightPos % boardHeight]
            [(boardWidthPos - 2) % boardWidth] != '.') {
        return 1;
    } else if (game->gameBoard[(boardHeightPos + 1) % boardHeight]
            [boardWidthPos % boardWidth] != '.') {
        return 1;
    } else if (game->gameBoard[(boardHeightPos - 1) % boardHeight]
            [boardWidthPos % boardWidth] != '.') {
        return 1;
    }
    return 0;
}

/*
 *Logic for the cpu1 player type. Puts card at first available location
 *from top left scrolling right then down to start of next line
 */
void cpu_player1_logic(struct GameDetails* game, int* width, int* height) {
    if (game->firstMove == 1) {
        *width = ((game->width / 2 + 1) / 2);
        *height = ((game->height + 1) / 2);
        game->firstMove = 0;
        return;
    }
    for (int i = 0; i < game->height; i++) {
        for (int j = 0; j < game->width; j += 2) {
            if (game->gameBoard[i][j] == '.') {
                if (check_adjacent(game, ((j / 2) + 1), i + 1)) {
                    *height = i + 1;
                    *width = ((j / 2) + 1);
                    return;
                }
            }
        }
    }
}

/*
 *Logic for the cpu2 player type. Puts card at first available location
 *from bottom right then scrolling left then up and end of next line.
 */
void cpu_player2_logic(struct GameDetails* game, int* width, int* height) {
    if (game->firstMove == 1) {
        *width = ((game->width / 2 + 1) / 2);
        *height = ((game->height + 1) / 2);
        game->firstMove = 0;
        return;
    }
    for (int i = game->height - 1; i >= 0; i--) {
        for (int j = game->width - 2; j >= 0; j -= 2) {
            if (game->gameBoard[i][j] == '.') {
                if (check_adjacent(game, ((j / 2) + 1), i + 1)) {
                    *height = i + 1;
                    *width = ((j / 2) + 1);
                    return;
                }
            }
        }
    }
}


/*
 *Iterates through the game board to see if there is an available
 *space to keep playing.
 */
bool check_free_space(struct GameDetails* game) {
    for (int i = 0; i < game->height; i++) {
        for (int j = 0; j < (game->width); j++) {
            if (game->gameBoard[i][j] == '.') {
                return true;
            }
        }
    }
    return false;
}


//Free things that need to be freed at the end of a game.
void free_game_stuff(struct GameDetails* game) {
    for (int i = 0; i < game->height; i++) {
        free(game->gameBoard[i]);
    }
    free(game->gameBoard);
    free_deck(&game->player1->hand);
    free_deck(&game->player2->hand);
    free_deck(&game->deck);
    free(game);
}

/*
 *This saves the current state of the game as a file named **** when calling
 *SAVE****.
 */
void save_game(struct GameDetails* game, char* saveGameName) {
    int size = 5;
    char* saveName = (char*)malloc(sizeof(char) * size);
    int i = 4;
    while (i < 80 && saveGameName[i] != '\0') {
        saveName[i - 4] = saveGameName[i];
        i++;
        if ((i - 4) == size) {
            size *= 2;
        }
    }
    saveName[i] = '\0';
    FILE* save = fopen(saveName, "w");
    if (save == NULL) {
        fprintf(stderr, "Unable to parse savefile\n");
        exit(4);
    }
    fprintf(save, "%d %d %d %d\n", (game->width / 2), game->height,
            game->cardsDrawn, game->currentPlayer);
    i = 0;
    while (game->deckFileName[i] != '\0') { 
        fputc(game->deckFileName[i], save);
        i++;
    }
    fprintf(save, "\n");
    print_player_hand_save(get_hand_string(&game->player1->hand), save);
    print_player_hand_save(get_hand_string(&game->player2->hand), save);
    print_board_save(game, save);
    fclose(save);
}


/*
 *Check to see that the first part of the saveFile command is SAVE
 */
int check_save_input(char* name) {
    if (name[0] == 'S') {
        if (name[1] == 'A') {
            if (name[2] == 'V') {
                if (name[3] == 'E') {
                    return 0;
                }
            }
        }
    }
    return 1;
}


//Prints the malloced gameboard to as a save file format
void print_board_save(struct GameDetails* game, FILE* fileType) {
    for (int i = 0; i < game->height; i++) {
        for (int j = 0; j < game->width; j++) {
            if (game->gameBoard[i][j] == '.') {
                fprintf(fileType, "*");
            } else {
                fprintf(fileType, "%c", game->gameBoard[i][j]);
            }
        }
        fprintf(fileType, "\n");
    }
}
