typedef struct Card* CardNode;

typedef struct Deck* ADeck;

struct Deck {
    CardNode head;
    CardNode tail;
};

struct Card {
    char cardNum;
    char cardSuit;
    struct Card* next;
    struct Card* previous;
};

void init_deck(ADeck deck);

void free_deck(ADeck deck);

void add_card(ADeck deck, char cardNum, char cardSuit);

int get_card_data(ADeck deck, char* cardNum, char* cardSuit);

int get_card_at_position(ADeck hand, int position,
        char* cardNum, char* cardSuit);

char* get_hand_string(ADeck deck);

void get_saved_hand_data(ADeck deck, char* handLine);
